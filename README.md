# Proyecto Software ELEL226

En el siguiente repositorio estan disponibles los archivos utilizados para la simulación y ejecución, para la creacion de un macetero inteligente que alerta las condiciones de vida de las plantas, denominado Smartcetero.


## Necesidad identificada:

El usuario normalmente no está preocupado de las condiciones ́optimas de vida de sus plantas, como son la humedad relativa del aire o su temperatura, por lo que es de gran ayuda un sistema que indique las condiciones de ambiente de sus plantas para que pueda proveerle un mejor cuidado.

## Propuesta:

Monitoreo de parametros de vida de una planta mediante el uso de sensores.

## Solucion Planteada: 

Diseñar un dispositivo electrónico capaz de medir la temperatura y humedad relativa del aire para indicar mediante mensajes de alerta al usuario sobre el estadode sus plantas.

## Metodologia:

Para que una planta pueda realizar su metabolismo sin mayores complicaciones, se necesita
que este dentro de los rangos del D ́eficit de presi ́on de vapor (DPV), de modo que se tengan
las condiciones de vida  ́optimas de las plantas, sin deshidratarse ni ahogarse. Para determinar
el DPV es necesario medir la Humedad relativa (RH) y la Temperatura del aire. Por lo tanto
se necesita un sensor para medir estos dos par ́ametros. (El DHT22 mide ambos datos)

Se empleara una Raspberry Pi 4 y codificado con Python.

Para la comunicacion de la Raspberry al usuario se emplear ́a el modulo WiFi integrado en la
tarjeta, donde enviara los datos procesados del c ́alculo del DPV a un sistema de alerta que
informe periodicamente al usuario del estado de sus plantas, en especifico, mediante mensajes
de whatsapp o sms al usuario.
