#!/usr/bin/env python
#Librerias a utilizar

import adafruit_dht
import time
import requests

#definimos el algoritmo de envio de mensajes por telegram
def send_to_telegram(message):
 
     apiToken = '5979191605:AAG7s4ItNLdlEr7JxKVaA17ucgbKb2wlf6w'
     chatID = '-846399896'
     apiURL = f'https://api.telegram.org/bot{apiToken}/sendMessage'
 
     try:
         response = requests.post(apiURL, json={'chat_id': chatID, 'text': message})
         print(response.text)
     except Exception as e:
         print(e)

#definicion del pin utilizado por el sensor:
dht_device = adafruit_dht.DHT22(4)

#inicializando valores de temperatura y humedad
temperature=0
humidity=0

while True: #ciclo while para obtener los datos del sensor.
    succes=False
    tries=15 #despues de 15 intentos, arrojara un mensaje de error
    while (succes == False and tries > 0):
        try:
            temperature = dht_device.temperature
            humidity    = dht_device.humidity
            if humidity is not None and temperature is not None: 
                succes=True
        except:
            tries-=1
            print ('Alerta: Error de lectura de sensor, intentos restantes:', tries)
            if (tries<1):
                raise

    T=float(temperature) #determinando las variables del modelo
    H_R=float(humidity)


    e=2.718281 #constante euler

    DPV=0.61078*(e**((17.27*T)/(237.3+T)))*(1-(H_R)/(100))#modelo del DPV

    print(round(DPV, 2), "KPa Deficit de Presión de Vapor")
    print(T, "°C Temperatura") 
    print(H_R, "% Humedad") 

    if DPV<0.4:#definicion de los distintos mensajes de alerta
        send_to_telegram("HOLA!, soy tu planta")
        send_to_telegram("El DPV es muy bajo.")
        send_to_telegram("Deberías aumentar la temperatura, TENGO FRIOOOOO")
    elif 1.4<DPV:
        send_to_telegram("HOLA!, soy tu planta")
        send_to_telegram("El DPV es muy alto.")
        send_to_telegram("Deberías disminuir la temperatura. HACE CALOR Y TENGO SED.")
        send_to_telegram("Sed de la peligrosa")

    elif 0.41<DPV<0.5:
        send_to_telegram("El DPV es de un valor medio")
        send_to_telegram("Creo que me sentiria mejor con un poquito más de temperatura")
    elif 1.22<DPV<1.39:
        send_to_telegram("El DPV es de un valor medio")
        send_to_telegram("Creo que necesito un pequeño refresco, enciende el aire acondicionado por favor")
    elif 0.51<DPV<1.2:
        send_to_telegram("Me siento bien. Estoy playa")
    else:
        send_to_telegram("No estoy midiendo bien. Ven a verme ")
    
    time.sleep(6) #paso del tiempo de notificacion
